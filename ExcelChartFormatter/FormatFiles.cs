﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using OfficeOpenXml.Drawing.Chart;
using System.ComponentModel;
using System.IO;
using OfficeOpenXml;
using System.Reflection;
using System.Xml;
using System.Diagnostics;

namespace FormatExcel
{
    public class FormatFiles
    {
        int wsCounter = 1;
        List<string> _oldFileNameList;
        string _newFileName;
        string _filesPath;
        Settings _sett;

        //fileList, aggregateFileName, _hostingEnvironment.WebRootPath + @"\Uploads\", model.ToDates, model.ToSlide        

        public FormatFiles(params object[] FormatSpec)
        {
            string templ;

            _oldFileNameList = (List<string>)FormatSpec[0];
            _newFileName = (string)FormatSpec[1];
            _filesPath = (string)FormatSpec[2];
            _sett = new Settings() { SetDateAxisMinMax = (string)FormatSpec[3], FormatToSlides = (string)FormatSpec[4] };      
                        
            if (_sett.FormatToSlides.ToLower().Equals("true"))
            {
                templ = "EPPlusStandard.Resources.tmplSlide.xlsx";
            }
            else
            {
                templ = "EPPlusStandard.Resources.tmpl.xlsx";
            }

            var resources = Assembly.GetExecutingAssembly().GetManifestResourceNames();

            using (Stream templateStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(templ))
            {
                using (FileStream newFileStream = new FileStream(_filesPath + _newFileName, FileMode.Create))
                {
                    using (ExcelPackage newPack = new ExcelPackage(newFileStream, templateStream, null))
                    {
                        foreach (var file in _oldFileNameList.Take(10))
                        {
                            if (File.Exists(_filesPath + file))
                            {                                
                                FileInfo oldFinfo = new FileInfo(_filesPath + file);

                                using (ExcelPackage oldPack = new ExcelPackage(oldFinfo))
                                {
                                    ExtractCharts(oldPack, newPack, _sett);
                                    oldPack.Dispose();
                                }
                            }
                        }

                        if (wsCounter == 0)
                            throw new ArgumentException("No charts were found in Excel file");

                        for (int i = 150; i >= wsCounter; i--)
                        {
                            newPack.Workbook.Worksheets.Delete(i.ToString());
                        }

                        newPack.Save();
                        newPack.Dispose();
                    }
                }
            }
        }     
        

        private void ExtractCharts(ExcelPackage oldPack, ExcelPackage newPack, Settings sett)
        {
            foreach (ExcelWorksheet wso in oldPack.Workbook.Worksheets)
            {
                if (wsCounter > 150 )
                    return;
                
                for (int d = 0; d < wso.Drawings.Count(); d++)
                {
                    if (wso.Drawings[d] != null && wso.Drawings[d] is ExcelChart)
                    {
                        ExcelChart oldChart = wso.Drawings[d] as ExcelChart;    
                        var newWorkSheet = newPack.Workbook.Worksheets[wsCounter.ToString()];

                        // ====  First language ====
                        ChartFormatter formatter = new ChartFormatter(oldChart, newWorkSheet, sett);                        
                        formatter.FormatSeriesLanguage1();
                        
                        var newChart1 = newPack.Workbook.Worksheets[wsCounter.ToString()].Drawings[0] as ExcelChart;
                        ReplaceChartType(oldChart, newChart1);



                        // ====  Second language ==========
                        var newChart2 = newPack.Workbook.Worksheets[wsCounter.ToString()].Drawings[1] as ExcelChart;
                        formatter.FormatSeriesLanguage2();

                        ReplaceChartType(oldChart, newChart2);
                    }

                    wsCounter++;
                }
            }
        }


        private void ReplaceChartType(ExcelChart oldCht, ExcelChart newCht)
        {
            var nChtXml = newCht.ChartXml;
            var oChtXml = oldCht.ChartXml;

            var nsa = newCht.NameSpaceManager.LookupNamespace("a");
            var nsuri = nChtXml.DocumentElement.NamespaceURI;
            var nsm = new XmlNamespaceManager(nChtXml.NameTable);
            nsm.AddNamespace("a", nsa);
            nsm.AddNamespace("c", nsuri);

            var newNode = nChtXml.SelectSingleNode($@"c:chartSpace/c:chart", nsm);
            var oldNode = oChtXml.SelectSingleNode($@"c:chartSpace/c:chart", nsm);

            var mm = newNode.ChildNodes;

            for (int i = newNode.ChildNodes.Count - 1; i >= 0; i--)
            {
                if (!newNode.ChildNodes[i].Name.Equals("c:extLst"))
                {
                    var x = newNode.ChildNodes[i].ParentNode;
                    x.RemoveChild(newNode.ChildNodes[i]);
                }
            }

            for (int i = 0; i < oldNode.ChildNodes.Count; i++)
            {
                if (!oldNode.ChildNodes[i].Name.Equals("c:extLst"))
                {
                    var node = oldNode.ChildNodes[i];
                    var ip = nChtXml.ImportNode(node, true);
                    newNode.AppendChild(newNode.OwnerDocument.ImportNode(ip, true));
                }
            }

        }
    }
}



