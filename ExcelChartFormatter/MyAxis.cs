﻿using OfficeOpenXml.Drawing.Chart;
using System;
using System.Collections.Generic;
using System.Text;

namespace EPPlusStandard.Models
{
    public class ExcelChartAxis
    {
        ExcelChartAxis _axis;

        public ExcelChartAxis(ExcelChartAxis Axis)
        {
            _axis = Axis;
        }

       public ExcelChartAxis GetEppAxis { get { return _axis; } }
               
    }
}
