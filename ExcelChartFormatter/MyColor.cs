﻿using OfficeOpenXml.Drawing.Chart;
using System;
using System.Collections.Generic;
using System.Text;

namespace EPPlusStandard.Models
{
    public class MyColor
    {
        public int Index { get; set; }

        public string Style { get; set; }

        public string Weight { get; set; }

        public string Color { get; set; }

        public bool ChartType(ExcelChart cht)
        {
            string style = "default";

            if (cht is ExcelLineChart)
            {
                style = "line";
            }

            if (cht is ExcelBarChart)
            {
                style = "column";
            }

            return Style.Equals(style);
        }       

    }
}
