﻿using EPPlusStandard.Models;
using EPPlusStandard.Services;
using OfficeOpenXml;
using OfficeOpenXml.Drawing.Chart;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;

namespace FormatExcel
{
    public class ColorsService : IService<MyColor>
    {
        private static List<MyColor> seriesProps = new List<MyColor>();
        

        public ColorsService Initialize()
        {
            // =============== Line properties =====================
            var pl = Assembly.GetExecutingAssembly().GetManifestResourceStream("EPPlusStandard.Resources.SeriesLineProperties.xml");
            ReadProperties(pl, "line");

            // =============== Column properties =====================
            var pc = Assembly.GetExecutingAssembly().GetManifestResourceStream("EPPlusStandard.Resources.SeriesColumnProperties.xml");
            ReadProperties(pc, "column");

            // =============== Default properties =====================
            var pd = Assembly.GetExecutingAssembly().GetManifestResourceStream("EPPlusStandard.Resources.SeriesColumnProperties.xml");
            ReadProperties(pd, "default");

            return this;

        }

        private void ReadProperties(Stream sm, string style)
        {
            XmlDocument docL = new XmlDocument();
            docL.Load(sm);

            for (int i = 0; i < docL.ToXDocument().Element("Series").Elements("SeriesProperties").Count(); i++)
            {
                var e = docL.ToXDocument().Element("Series").Elements("SeriesProperties").ElementAt(i);

                MyColor sp = new MyColor();
                sp.Style = style;
                sp.Index = int.Parse(e.Element("Index").Value);
                sp.Weight = e.Element("Weight").Value;
                sp.Color = e.Element("Color").Value;

                seriesProps.Add(sp);
            }

        }       

        public IList<MyColor> GetAll()
        {
            return seriesProps;
        }

        public MyColor Get(int idx)
        {
            return null;
            //seriesProps.Where(x => x.Index == idx && x.Style.Equals(style)).FirstOrDefault();

        }

    }
}


//public SeriesColor GetColor(int idx, ExcelChart cht)
//{
//    string style = "default";

//    if (cht is ExcelLineChart)
//    {
//        style = "line";
//    }

//    if (cht is ExcelBarChart)
//    {
//        style = "column";
//    }

//    var p = seriesProps.Where(x => x.Index == idx && x.Style.Equals(style)).FirstOrDefault();

//    if (p != null)
//    {
//        return p;
//    }

//    return seriesProps.Last();
//}




