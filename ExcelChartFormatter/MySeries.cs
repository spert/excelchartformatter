﻿using EPPlusStandard.Models;
using OfficeOpenXml;
using OfficeOpenXml.Drawing.Chart;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using static FormatExcel.Extensions;
using ExcelChartAxis = OfficeOpenXml.Drawing.Chart.ExcelChartAxis;

namespace FormatExcel
{
    public class MySeries
    {
        ExcelWorksheet _newWs;
        ExcelChart _oldChart;
        ExcelChartSerie _oldSer;
        MyColor _format;
        bool _printLeftRight;
        Settings _sett;

        public MySeries(Settings Settings)
        {
            _sett = Settings;
        }

        public ExcelChart OldChart { get { return _oldChart; } set { _oldChart = value; } }

        public ExcelWorksheet NewWorkSheet { get { return _newWs; } set { _newWs = value; } }

        public ExcelChartSerie OldSerie { get { return _oldSer; } set { _oldSer = value; } }

        public MyColor Color { get { return _format; } set { _format = value; } }

        public bool PrintLeftRightText { get { return _printLeftRight; } set { _printLeftRight = value; } }

        public MyExcelRange SerHeadingLang1FormulaRange { get; set; }

        public MyExcelRange SerHeadingLang2FormulaRange { get; set; }

        public MyExcelRange SerValues { get; set; }

        public MyCategory SerCategory { get; set; }

        public List<ExcelChartAxis> Axes { get; set; }

        public void SetColors()
        {
            if (_oldChart is ExcelBarChart)
            {
                _oldSer.DeleteNode("c:spPr");
                _oldSer.CreateComplexNode("c:spPr/a:solidFill/a:srgbClr/@val='" + _format.Color + "'");
                _oldSer.CreateComplexNode("c:spPr/a:ln/@w='0'/a:solidFill/a:srgbClr/@val='" + _format.Color + "'");
            }
            else if (_oldChart is ExcelLineChart)
            {
                _oldSer.DeleteNode("c:spPr");
                _oldSer.CreateComplexNode("c:spPr/a:ln/@w='" + _format.Weight + "'/@cap=rnd/a:solidFill/a:srgbClr/@val='" + _format.Color + "'");
            }
        }

        public void RemoveMarkers()
        {
            if (_oldChart is ExcelLineChart)
            {
                _oldSer.DeleteAllNode("c:marker");
                _oldSer.CreateComplexNode("c:marker/c:symbol/@val='none'");
            }
        }

        public void SetCategoriesReferences()
        {
            if (SerCategory is MyDateCategory || SerCategory is MyNumericCategory)
                _oldSer.SetXmlNodeString("c:cat/c:numRef/c:f", SerCategory.MyNewRange.EppRange.FullAddressAbsolute);

            if (SerCategory is MyStrCategory)
                _oldSer.SetXmlNodeString("c:cat/c:strRef/c:f", SerCategory.MyNewRange.EppRange.FullAddressAbsolute);

            if (SerCategory is MyMultilevCategory)
                _oldSer.SetXmlNodeString("c:cat/c:multiLvlStrRef/c:f", SerCategory.MyNewRange.EppRange.FullAddressAbsolute);
        }

        public void SetHeadingReferences(ref int column)
        {
            string h = "";

            var vHead = _oldSer.TopNode.SelectSingleNode("c:tx/c:v", _oldSer.NameSpaceManager);

            if (vHead != null)
            {
                h = vHead.InnerText;
            }

            var refHead = _oldSer.TopNode.SelectSingleNode("c:tx/c:strRef", _oldSer.NameSpaceManager);

            if (refHead != null)
            {
                var pts = refHead.SelectNodes("c:strCache/c:pt", _oldSer.NameSpaceManager);

                foreach (XmlNode node in pts)
                {
                    int idx = int.Parse(node.Attributes["idx"].InnerText);
                    var v = node.SelectSingleNode("c:v", _oldSer.NameSpaceManager);
                    h = h + " " + v.InnerText;
                }
            }

            _newWs.Cells[1, column].Value = h;
            _newWs.Cells[2, column].Value = "<translate>";

            if (PrintLeftRightText && Axes.Where(x => x.GetXmlNodeString("c:axPos/@val").Equals("r")).Count() > 0)
            {
                _newWs.Cells[3, column].Value = "(parem telg)";
                _newWs.Cells[4, column].Value = "(right scale)";
            }
            else if (PrintLeftRightText && Axes.Where(x => x.GetXmlNodeString("c:axPos/@val").Equals("l")).Count() > 0)
            {
                _newWs.Cells[3, column].Value = "(vasak telg)";
                _newWs.Cells[4, column].Value = "(left scale)";
            }
            else if (!PrintLeftRightText)
            {
                _newWs.Cells[3, column].Value = "";
                _newWs.Cells[4, column].Value = "";
            }

            _newWs.Cells[7, column].Formula = _newWs.Cells[1, column].FullAddress + "&\"" + " \"" + "&" + _newWs.Cells[3, column].FullAddress;
            _newWs.Cells[8, column].Formula = _newWs.Cells[2, column].FullAddress + "&\"" + " \"" + "&" + _newWs.Cells[4, column].FullAddress;

            _oldSer.DeleteAllNode("c:tx/c:v");
            _oldSer.DeleteAllNode("c:tx/c:strRef/c:strCache");
            _oldSer.SetXmlNodeString("c:tx/c:strRef/c:f", _newWs.Cells[7, column].FullAddressAbsolute);            
            _oldSer.CreateComplexNode("c:tx/c:strRef/c:strCache/c:ptCount/@val='1'");
            _oldSer.CreateComplexNode("c:tx/c:strRef/c:strCache/c:pt/@idx='0'/c:v");
            _oldSer.SetXmlNodeString("c:tx/c:strRef/c:strCache/c:pt/c:v", _newWs.Cells[1, column].Value + " " + _newWs.Cells[3, column].Value);

            SerHeadingLang1FormulaRange = new MyExcelRange(_newWs.Cells[7, column], null);
            SerHeadingLang2FormulaRange = new MyExcelRange(_newWs.Cells[8, column], null);                     

        }

        public void SetHeadingReferencesLang2()
        {
            _oldSer.DeleteAllNode("c:tx/c:v");
            _oldSer.DeleteAllNode("c:tx/c:strRef/c:strCache");
            _oldSer.SetXmlNodeString("c:tx/c:strRef/c:f", SerHeadingLang2FormulaRange.EppRange.FullAddressAbsolute);
            _oldSer.CreateComplexNode("c:tx/c:strRef/c:strCache/c:ptCount/@val='1'");
            _oldSer.CreateComplexNode("c:tx/c:strRef/c:strCache/c:pt/@idx='0'/c:v");
            _oldSer.SetXmlNodeString("c:tx/c:strRef/c:strCache/c:pt/c:v", "<translate>");
        }

        public void SetSeriesValues(ref int column)
        {
            int vCount = 0;
            object[] sValues = new object[0];

            var refType = _oldSer.TopNode.SelectSingleNode("c:val/c:numRef", _oldSer.NameSpaceManager);

            if (refType != null)
            {
                string formatCode = _oldSer.TopNode.SelectSingleNode("c:val/c:numRef/c:numCache/c:formatCode", _oldSer.NameSpaceManager).InnerText;
                vCount = int.Parse(_oldSer.TopNode.SelectSingleNode("c:val/c:numRef/c:numCache/c:ptCount/@val", _oldSer.NameSpaceManager).InnerText);

                if (vCount > 0)
                {
                    sValues = new object[vCount];
                    var pts = _oldSer.TopNode.SelectNodes("c:val/c:numRef/c:numCache/c:pt", _oldSer.NameSpaceManager);

                    foreach (XmlNode node in pts)
                    {
                        int idx = int.Parse(node.Attributes["idx"].InnerText);
                        var v = node.SelectSingleNode("c:v", _oldSer.NameSpaceManager);
                        sValues[idx] = v.InnerText;
                    }

                    SerValues = new MyExcelRange(_newWs.Cells[9, column, 9 + vCount - 1, column], formatCode);
                    _oldSer.SetXmlNodeString("c:val/c:numRef/c:f", SerValues.EppRange.FullAddressAbsolute);

                }
            }

            var litType = _oldSer.TopNode.SelectSingleNode("c:val/c:numLit", _oldSer.NameSpaceManager);

            if (litType != null)
            {
                //formatCode = _oldSer.TopNode.SelectSingleNode("c:val/c:numLit/c:formatCode", _oldSer.NameSpaceManager).InnerText;
                vCount = int.Parse(_oldSer.TopNode.SelectSingleNode("c:val/c:numLit/c:ptCount/@val", _oldSer.NameSpaceManager).InnerText);

                if (vCount > 0)
                {
                    sValues = new object[vCount];
                    var pts = _oldSer.TopNode.SelectNodes("c:val/c:numLit/c:pt", _oldSer.NameSpaceManager);

                    foreach (XmlNode node in pts)
                    {
                        int idx = int.Parse(node.Attributes["idx"].InnerText);
                        var v = node.SelectSingleNode("c:v", _oldSer.NameSpaceManager);
                        sValues[idx] = v.InnerText;
                    }
                }
            }

            for (int i = 0; i < vCount; i++)
            {
                if (sValues[i] is null)
                {
                    _newWs.Cells[9 + i, column].Value = "";
                }
                else if (sValues[i].ToString().Equals("#N/A"))
                {
                    _newWs.Cells[9 + i, column].Formula = "#N/A";
                }
                else
                {
                    double d;

                    if (double.TryParse(sValues[i].ToString(), out d))
                    {
                        _newWs.Cells[9 + i, column].Value = d;
                    }
                    else
                    {
                        _newWs.Cells[9 + i, column].Value = sValues[i];
                    }
                }
            }

            SerValues.EppRange.Style.Numberformat.Format = SerValues.NumberFormat();
        }



        public List<string> GetAxisId()
        {
            var nodes = _oldSer.TopNode.ParentNode.SelectNodes("c:axId/@val", _oldSer.NameSpaceManager).Cast<XmlAttribute>().Select(x => x.Value).ToList();

            return nodes;
        }

        public string GetChtType()
        {
            if (_oldChart is ExcelLineChart)
            {
                return "line";
            }

            if (_oldChart is ExcelBarChart)
            {
                return "column";
            }

            return "default";
        }

        public bool GetIsStacked()
        {
            var c = _oldChart as ExcelBarChart;

            if (c.Grouping == eGrouping.Stacked || c.Grouping == eGrouping.PercentStacked)
            {
                return true;
            }

            return false;
        }

        public void RemoveExternalRefs()
        {
            _oldSer.DeleteNode("c:extLst");
        }

        public void MaxMinDatXAxis()
        {
            if (!_sett.SetDateAxisMinMax.ToLower().Equals("true"))
                return;

            var datAx = Axes.Where(x => !x.Deleted && x.TopNode.Name.Equals("c:dateAx")).FirstOrDefault();

            if (datAx != null && SerCategory is MyDateCategory)
            {
                var cat = SerCategory as MyDateCategory;

                datAx.MaxValue = cat.DMax.Value.ToOADate();
                datAx.MinValue = cat.DMin.Value.ToOADate();
                datAx.MajorUnit = cat.DMajorUnit;
                datAx.MinorUnit = cat.DMajorUnit;
                datAx.MinorTimeUnit = cat.MajorTimeUnit;
                datAx.MajorTimeUnit = cat.MajorTimeUnit;
                datAx.Format = cat.FormatCode;
                datAx.SourceLinked = false;

                datAx.DeleteNode("c:baseTimeUnit");
                datAx.CreateComplexNode("c:baseTimeUnit/@val=" + cat.BaseTimeUnit.ToString().ToLower());
            }
        }
    }
}
