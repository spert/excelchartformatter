﻿using EPPlusStandard.Models;
using FormatExcel;
using OfficeOpenXml;
using OfficeOpenXml.Drawing.Chart;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Xml;

namespace EPPlusStandard.Services
{
    public class CategoryService : IService<MyCategory>
    {
        List<MyCategory>  _catLst = new List<MyCategory>();
        ExcelChart _oldChart;
        ExcelWorksheet _newWorkSheet;
        Settings _sett;
        int _maxLenght = 0;

        public CategoryService(ExcelChart OldChart, ExcelWorksheet NewWorkSheet, Settings Settings)
        {
            _oldChart = OldChart;
            _newWorkSheet = NewWorkSheet;
            _sett = Settings;
        }

        public CategoryService Initialize()
        {
            var ct = _oldChart.PlotArea.ChartTypes.Select(x => x.GetHashCode()).Distinct();

            foreach (var ctH in ct)
            {
                var chtTyp = _oldChart.PlotArea.ChartTypes.Where(x => x.GetHashCode().Equals(ctH)).First();
                
                foreach (ExcelChartSerie serie in chtTyp.Series)
                {
                    // Numeric values
                    XmlNode oldNumRef = serie.TopNode.SelectSingleNode("c:cat/c:numRef/c:f", _oldChart.NameSpaceManager);

                    if (oldNumRef != null && _catLst.Where(x => x.OldRangeStr.Equals(oldNumRef.InnerText)).Count() == 0)
                    {
                         var cValues = GetNumericCategoriesValues(serie);
                        GetMaxStringLenghtOfColumns(cValues);

                        string formatString = serie.TopNode.SelectSingleNode("c:cat/c:numRef/c:numCache/c:formatCode", serie.NameSpaceManager).InnerText;

                        if (formatString.Equals("m/d/yyyy"))
                        {
                            var maxMin = Extensions.SetDateMaxMin(cValues);

                            MyExcelRange newRange = new MyExcelRange(_newWorkSheet, 9, 7 + _catLst.Sum(x=>x.GetColumnsCount()), 9 + cValues.GetLength(0) - 1, 7 + cValues.GetLength(1) - 1, maxMin.Item5);
                            MyDateCategory catDate = new MyDateCategory(newRange, oldNumRef.InnerText);
                            catDate.Frequency = maxMin.Item1;
                            catDate.DMin = maxMin.Item2;
                            catDate.DMax = maxMin.Item3;
                            catDate.DMajorUnit = maxMin.Item4;
                            catDate.FormatCode = maxMin.Item5;
                            catDate.MajorTimeUnit = maxMin.Item6;
                            catDate.BaseTimeUnit = maxMin.Item7;

                            PrintCategoriesValues(catDate, cValues);
                            _catLst.Add(catDate);

                        }
                        else
                        {
                            MyExcelRange newRange = new MyExcelRange(_newWorkSheet, 9, 7 + _catLst.Sum(x => x.GetColumnsCount()), 9 + cValues.GetLength(0) - 1, 7 + cValues.GetLength(1) - 1, formatString);
                            MyNumericCategory catNum = new MyNumericCategory(newRange, oldNumRef.InnerText);
                            PrintCategoriesValues(catNum, cValues);
                            _catLst.Add(catNum);
                        }
                    }


                    // String values
                    XmlNode oldStrRef = serie.TopNode.SelectSingleNode("c:cat/c:strRef/c:f", _oldChart.NameSpaceManager);

                    if (oldStrRef != null && _catLst.Where(x => x.OldRangeStr.Equals(oldStrRef.InnerText)).Count() == 0)
                    {
                        var cValues = GetStringCategoriesValues(serie);
                        GetMaxStringLenghtOfColumns(cValues);

                        MyExcelRange newRange = new MyExcelRange(_newWorkSheet, 9, 7 + _catLst.Sum(x => x.GetColumnsCount()), 9 + cValues.GetLength(0) - 1, 7 + cValues.GetLength(1) - 1, null);
                        MyStrCategory catStr = new MyStrCategory(newRange, oldStrRef.InnerText);
                        PrintCategoriesValues(catStr, cValues);
                        _catLst.Add(catStr);
                    }

                    // Multilevel values
                    XmlNode oldMultRef = serie.TopNode.SelectSingleNode("c:cat/c:multiLvlStrRef/c:f", _oldChart.NameSpaceManager);

                    if (oldMultRef != null && _catLst.Where(x => x.OldRangeStr.Equals(oldMultRef.InnerText)).Count() == 0)
                    {
                        var cValues = GetMultCategoriesValues(serie);
                        GetMaxStringLenghtOfColumns(cValues);

                        MyExcelRange newRange = new MyExcelRange(_newWorkSheet, 9, 7 + _catLst.Sum(x => x.GetColumnsCount()), 9 + cValues.GetLength(0) - 1, 7 + _catLst.Sum(x => x.GetColumnsCount()) + cValues.GetLength(1) - 1, null);
                        MyMultilevCategory catMultilev = new MyMultilevCategory(newRange, oldMultRef.InnerText);
                        PrintCategoriesValues(catMultilev, cValues);
                        _catLst.Add(catMultilev);
                    }
                }
            }

            return this;
        }

        private void PrintCategoriesValues(MyCategory cat, object[,] cValues)
        {
            int row = cat.MyNewRange.EppRange.Start.Row;
            int column = cat.MyNewRange.EppRange.Start.Column;
            ExcelRange rng = new ExcelRange(cat.MyNewRange.EppRange.Worksheet, row, column, row, column);

            for (int i = 0; i < cValues.GetLength(0); i++)
            {
                for (int j = 0; j < cValues.GetLength(1); j++)
                {
                    if (cValues[i, j] is null)
                    {
                        rng[row + i, column + j].Value = "";
                    }
                    else if (cValues[i, j].ToString().Equals("#N/A"))
                    {
                        rng[row + i, column + j].Formula = "#N/A";
                        rng[row + i, column + j].Style.Numberformat.Format = "0.0";
                    }
                    else
                    {
                        double d;

                        if (double.TryParse(cValues[i, j].ToString(), out d))
                        {
                            rng[row + i, column + j].Value = d;
                        }
                        else
                        {
                            rng[row + i, column + j].Value = cValues[i, j];
                        }

                        if (cat.FormatCode != null)
                        {
                            rng[row + i, column + j].Style.Numberformat.Format = cat.FormatCode;
                        }
                    }
                }
            }
        }

        //private void PrintStringCategoriesValues(MyCategory cat, object[,] cValues)
        //{
        //    int row = cat.MyNewRange.EppRange.Start.Row;
        //    int column = cat.MyNewRange.EppRange.Start.Column;
        //    ExcelRange rng = new ExcelRange(cat.MyNewRange.EppRange.Worksheet, row, column, row, column);

        //    for (int i = 0; i < cValues.GetLength(0); i++)
        //    {
        //        for (int j = 0; j < cValues.GetLength(1); j++)
        //        {
        //            if (cValues[i, j] is null)
        //            {
        //                rng[row + i, column + j].Value = "";
        //            }
        //            else if (cValues[i, j].ToString().Equals("#N/A"))
        //            {
        //                rng[row + i, column + j].Formula = "#N/A";
        //            }
        //            else
        //            {
        //                rng[row + i, column + j].Value = cValues[i, j].ToString();
        //            }
        //        }
        //    }
        //}

        private object[,] GetNumericCategoriesValues(ExcelChartSerie Serie)
        {
            int cCountRow = 0;
            int cCountColumn = 0;
            object[,] cValues = new object[0, 0];
            
            cCountRow = int.Parse(Serie.TopNode.SelectSingleNode("c:cat/c:numRef/c:numCache/c:ptCount/@val", Serie.NameSpaceManager).InnerText);
            cCountColumn = 1;

            if (cCountRow > 0)
            {
                cValues = new object[cCountRow, 1];
                var pts = Serie.TopNode.SelectNodes("c:cat/c:numRef/c:numCache/c:pt", Serie.NameSpaceManager);

                foreach (XmlNode pt in pts)
                {
                    int idx = int.Parse(pt.Attributes["idx"].InnerText);
                    var v = pt.SelectSingleNode("c:v", Serie.NameSpaceManager);

                    cValues[idx, 0] = pt.InnerText;
                }
            }            

            return cValues;

        }

        private object[,] GetStringCategoriesValues(ExcelChartSerie Serie)
        {
            int cCountRow = 0;
            int cCountColumn = 0;
            object[,] cValues = new object[0, 0];

            cCountRow = int.Parse(Serie.TopNode.SelectSingleNode("c:cat/c:strRef/c:strCache/c:ptCount/@val", Serie.NameSpaceManager).InnerText);
            cCountColumn = 1;

            if (cCountRow > 0)
            {
                cValues = new object[cCountRow, 1];
                var pts = Serie.TopNode.SelectNodes("c:cat/c:strRef/c:strCache/c:pt", Serie.NameSpaceManager);

                foreach (XmlNode pt in pts)
                {
                    int idx = int.Parse(pt.Attributes["idx"].InnerText);
                    var v = pt.SelectSingleNode("c:v", Serie.NameSpaceManager);

                    cValues[idx, 0] = pt.InnerText;
                }
            }

            return cValues;

        }

        private object[,] GetMultCategoriesValues(ExcelChartSerie Serie)
        {
            int cCountRow = 0;
            int cCountColumn = 0;
            object[,] cValues = new object[0, 0];

            cCountRow = int.Parse(Serie.TopNode.SelectSingleNode("c:cat/c:multiLvlStrRef/c:multiLvlStrCache/c:ptCount/@val", Serie.NameSpaceManager).InnerText);
            cCountColumn = Serie.TopNode.SelectNodes("c:cat/c:multiLvlStrRef/c:multiLvlStrCache/c:lvl", Serie.NameSpaceManager).Count;

            if (cCountRow > 0 && cCountColumn > 0)
            {
                cValues = new object[cCountRow, cCountColumn];
                var levels = Serie.TopNode.SelectNodes("c:cat/c:multiLvlStrRef/c:multiLvlStrCache/c:lvl", Serie.NameSpaceManager);

                int c = 0;

                foreach (XmlNode lvl in levels)
                {
                    foreach (XmlNode pt in lvl.SelectNodes("c:pt", Serie.NameSpaceManager))
                    {
                        int idx = int.Parse(pt.Attributes["idx"].InnerText);
                        var v = pt.SelectSingleNode("c:v", Serie.NameSpaceManager);

                        cValues[idx, c] = pt.InnerText;
                    }

                    c++;
                }
            }

            return cValues;

        }

        private void GetMaxStringLenghtOfColumns(object[,] cValues)
        {
            //int maxLenght = 0;

            //for (int i = 0; i < cValues.GetLength(0); i++)
            //{
            //    string s = "";

            //    for (int j = 0; j < cValues.GetLength(1); j++)
            //    {
            //        s = s + cValues[i, j] != null ? cValues[i, j].ToString() : "";
            //    }

            //    maxLenght = Math.Max(maxLenght, s.Length);
            //}

            //_maxLenght = maxLenght;
        }


        public int GetCatAxTextLength()
        {
           return _maxLenght;
        }

        public IList<MyCategory> GetAll()
        {
            return _catLst;
        }

        public MyCategory Get(int id)
        {
            throw new NotImplementedException();
        }

    }
}
