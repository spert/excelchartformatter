﻿using OfficeOpenXml.Drawing.Chart;
using System;
using System.Collections.Generic;
using System.Text;

namespace EPPlusStandard.Models
{
    public class MyDateCategory : MyCategory
    {
        public MyDateCategory(MyExcelRange NewRange, string OldRangeStr) : base (NewRange, OldRangeStr)
        {

        }

        public DateTime? DMin { get; set; }

        public DateTime? DMax { get; set; }

        public double? DMajorUnit { get; set; }

        public eTimeUnit? MajorTimeUnit { get; set; }

        public eTimeUnit? BaseTimeUnit { get; set; }

        public override string FormatCode { get; set; }

        public string Frequency { get; set; }

    }    
}
