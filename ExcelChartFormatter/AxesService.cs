﻿using EPPlusStandard.Models;
using FormatExcel;
using OfficeOpenXml.Drawing.Chart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using ExcelChartAxis = OfficeOpenXml.Drawing.Chart.ExcelChartAxis;

namespace EPPlusStandard.Services
{
    public class AxesService : IService<ExcelChartAxis>
    {
        List<ExcelChartAxis> _axes = new List<ExcelChartAxis>();
        Settings _sett;

        public AxesService Initialize(ExcelChart OldChart, Settings Settings)
        {
            _sett = Settings;

            var ct = OldChart.PlotArea.ChartTypes.Select(x => x.GetHashCode()).Distinct();

            foreach (var ctH in ct)
            {
                var chtTyp = OldChart.PlotArea.ChartTypes.Where(x => x.GetHashCode().Equals(ctH)).First();

                foreach (var ax in chtTyp.Axis)
                {
                    if (_axes.Where(x => x.Id.Equals(ax.Id)).Count() == 0)
                    {
                        _axes.Add(ax);
                    }
                }
            }

            return this;
        }

        public IList<ExcelChartAxis> GetAll()
        {
            return _axes;
        }

        public IList<ExcelChartAxis> Get(List<string> id)
        {
            return _axes.Where(x => id.Contains(x.Id)).ToList();
        }

        public ExcelChartAxis Get(int id)
        {
            throw new NotImplementedException();
        }


        public void SetCategoryAxesCrossing()
        {
            foreach (var ax in _axes.Where(x => x.TopNode.Name.Equals("c:catAx") || x.TopNode.Name.Equals("c:dateAx")))
            {
                CrossesYAt(ax);
            }           
        }

       
        private void CrossesYAt(ExcelChartAxis ax)
        {
            if (ax.TopNode.Name.Equals("c:valAx"))
                return;

            var cAx = ax.TopNode.SelectNodes("c:crossAx/@val", ax.NameSpaceManager);

            foreach (XmlNode a in cAx)
            {
                if (ax.AxisPosition == eAxisPosition.Bottom)
                {
                    var yl = _axes.Where(h => h.Id.Equals(a.Value) && h.TopNode.Name.Equals("c:valAx") && h.AxisPosition == eAxisPosition.Left).FirstOrDefault();

                    if (yl != null)
                    {
                        ax.CrossesAt = -9999999; //EPPlus.MinValue;
                    }
                    else
                    {
                        var yr = _axes.Where(h => h.Id.Equals(a.Value) && h.TopNode.Name.Equals("c:valAx") && h.AxisPosition == eAxisPosition.Right).FirstOrDefault();

                        if (yr != null)
                        {
                            ax.CrossesAt = -9999999; //EPPlus.MinValue;
                        }
                    }
                }

                if (ax.AxisPosition == eAxisPosition.Left)
                {
                    var yl = _axes.Where(h => h.Id.Equals(a.Value) && h.TopNode.Name.Equals("c:valAx") && h.AxisPosition == eAxisPosition.Bottom).FirstOrDefault();

                    if (yl != null)
                    {
                        ax.CrossesAt = -9999999; //EPPlus.MinValue;
                    }
                    else
                    {
                        var yr = _axes.Where(h => h.Id.Equals(a.Value) && h.TopNode.Name.Equals("c:valAx") && h.AxisPosition == eAxisPosition.Top).FirstOrDefault();

                        if (yr != null)
                        {
                            ax.CrossesAt = -9999999; //EPPlus.MinValue;
                        }
                    }
                }

            }
        }

        public void FormatAxisTitle()
        {
            //foreach (ExcelChartAxis ax in _axes.Where(x => x.TopNode.Name.Equals("c:valAx")))
            //{
            //    ax.DeleteNode("c:title/c:tx/c:rich/a:bodyPr");
            //    ax.CreateComplexNode("c:title/c:tx/c:rich/a:bodyPr/@anchorCtr='1'/@anchor='ctr'/@wrap='square'/@vert='horz'/@vertOverflow='ellipsis'/@spcFirstLastPara='1'/@rot='-5400000'");
            //    ax.DeleteNode("c:title/c:tx/c:rich/a:lstStyle");
            //    ax.CreateComplexNode("c:title/c:tx/c:rich/a:lstStyle");
            //    ax.DeleteNode("c:title/c:tx/c:rich/a:p/a:pPr");
            //    ax.CreateComplexNode("c:title/c:tx/c:rich/a:p/a:pPr/a:defRPr/@baseline='0'/@kern='1200'/@strike='noStrike'/@u='none'/@i='0'/@b='0'/@sz='1000'");
            //    ax.CreateComplexNode("c:title/c:tx/c:rich/a:p/a:pPr/a:defRPr/a:solidFill/a:schemeClr/@val='tx1'/a:lumMod/@val='65000'");
            //    ax.CreateComplexNode("c:title/c:tx/c:rich/a:p/a:pPr/a:defRPr/a:solidFill/a:schemeClr/@val='tx1'/a:lumOff/@val='35000'");
            //    ax.CreateComplexNode("c:title/c:tx/c:rich/a:p/a:pPr/a:defRPr/a:latin/@typeface='+mn-lt'");
            //    ax.CreateComplexNode("c:title/c:tx/c:rich/a:p/a:pPr/a:defRPr/a:ea/@typeface='+mn-ea'");
            //    ax.CreateComplexNode("c:title/c:tx/c:rich/a:p/a:pPr/a:defRPr/a:cs/@typeface='+mn-cs'");
            //    ax.DeleteNode("c:title/c:tx/c:rich/a:p/a:endParaRPr");
            //    ax.CreateComplexNode("c:title/c:tx/c:rich/a:p/a:endParaRPr/@lang='et-EE'");
            //}

        }
    }
}


//public void SetDateAxesMinMax(List<MySeries> MySeriesList)
//{
//    foreach (var ax in _axes.Where(x => x.TopNode.Name.Equals("c:dateAx")))
//    {
//        if (_sett.SetDateAxisMinMax.ToLower().Equals("true"))
//        {
//            //MaxMinDatXAxis(MySeriesList, ax);
//        }
//    }
//}


//private void MaxMinDatXAxis(List<MySeries> MySeriesList,  ExcelChartAxis ax)
//{
//    if (!ax.TopNode.Name.Equals("c:dateAx") && !ax.Deleted)
//        return;

//    var d = MySeriesList.Where(x => x.GetAxisId().Contains(ax.Id)).FirstOrDefault();

//    if (d != null)
//    {
//        ax.MaxValue = d.DatXAxisScale.dMax.Value.ToOADate();
//        ax.MinValue = d.DatXAxisScale.Dmin.Value.ToOADate();
//        ax.MajorUnit = d.DatXAxisScale.dMajorUnit;
//        ax.MinorUnit = d.DatXAxisScale.dMajorUnit;
//        ax.MinorTimeUnit = d.DatXAxisScale.majorTimeUnit;
//        ax.MajorTimeUnit = d.DatXAxisScale.majorTimeUnit;
//        ax.Format = d.DatXAxisScale.formatCode;
//        ax.SourceLinked = false;

//        ax.DeleteNode("c:baseTimeUnit");
//        ax.CreateComplexNode("c:baseTimeUnit/@val=" + d.DatXAxisScale.baseTimeUnit.ToString().ToLower());
//    }
//}