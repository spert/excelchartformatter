﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using EPPlusStandard.Models;
using EPPlusStandard.Services;
using OfficeOpenXml;
using OfficeOpenXml.Drawing.Chart;
using static FormatExcel.Extensions;

namespace FormatExcel
{  

    public partial class ChartFormatter
    {
        ExcelChart _oldChart;
        ExcelWorksheet _newWorkSheet;

        List<MySeries> _series = new List<MySeries>();
        AxesService _axes;
        CategoryService _categories;
        ColorsService _seriesColor;
        Settings _sett;
        int _column;

        public ChartFormatter(ExcelChart OldChart, ExcelWorksheet NewWorkSheet, Settings Sett)
        {
            _oldChart = OldChart;
            _newWorkSheet = NewWorkSheet;
            _sett = Sett;
            _axes = new AxesService().Initialize(_oldChart, _sett);
            _categories = new CategoryService(_oldChart, _newWorkSheet, _sett).Initialize();
            _seriesColor = new ColorsService().Initialize();
            _column = 7 + _categories.GetAll().Sum(x => x.GetColumnsCount());
        }        
        
        public ExcelChart OldChart { get { return _oldChart; } private set { } }  

        public ExcelWorksheet NewWorkSheet { get { return _newWorkSheet; } private set { } }        

        public void FormatSeriesLanguage1()
        {
            var ct = _oldChart.PlotArea.ChartTypes.Select(x => x.GetHashCode()).Distinct();
                        
            foreach (var ctH in ct)
            {
                var chtTyp = _oldChart.PlotArea.ChartTypes.Where(x => x.GetHashCode().Equals(ctH)).First();

                //Debug.Print("=============");
                //Debug.Print(chtTyp.WorkSheet.Name);
                //Debug.Print("=============");

                foreach (ExcelChartSerie _serie in chtTyp.Series)
                {
                    Debug.Print(_serie.Series);

                    MySeries s = new MySeries(_sett);

                    s.NewWorkSheet = _newWorkSheet;
                    s.OldChart = chtTyp;
                    s.OldSerie = _serie;

                    // Axes
                    List<string> axesId = _serie.TopNode.ParentNode.SelectNodes("c:axId/@val", chtTyp.NameSpaceManager).Cast<XmlNode>().Select(y => y.Value).ToList();
                    s.Axes = _axes.GetAll().Where(x => axesId.Contains(x.Id)).ToList();

                    //Left of Right Axis
                    s.PrintLeftRightText = _axes.GetAll().Where(x => x.AxisPosition == eAxisPosition.Left || x.AxisPosition == eAxisPosition.Right).Count() > 1 ? true : false;

                    //Series colors
                    s.Color = _seriesColor.GetAll().Where(x => x.Index == _series.Where(c => c.GetChtType().Equals(chtTyp.GetChartType())).Count() + 1 && x.ChartType(chtTyp))
                               .DefaultIfEmpty(_seriesColor.GetAll().Where(z => z.ChartType(chtTyp)).Last()).FirstOrDefault();

                    //Categories
                    List<string> categories = _serie.TopNode.SelectSingleNode("c:cat/*/c:f", _serie.NameSpaceManager).Cast<XmlNode>().Select(y => y.Value).ToList();
                    s.SerCategory = _categories.GetAll().Where(x => categories.Contains(x.OldRangeStr)).FirstOrDefault();

                    //Set categories references
                    s.SetCategoriesReferences();

                    //Set headings references
                    s.SetHeadingReferences(ref _column);

                    //Set values references
                    s.SetSeriesValues(ref _column); // keep order 

                    //Set max min values of date axis
                    s.MaxMinDatXAxis();

                    s.RemoveExternalRefs();
                    s.SetColors();
                    s.RemoveMarkers();

                    _column++;
                    _series.Add(s);
                }
            }

            // Axes Crossings
            _axes.SetCategoryAxesCrossing();
            _axes.FormatAxisTitle();

            if (_sett.FormatToSlides.ToLower().Equals("true"))
            {
                FormatAxesLayoutSlide();
                RemoveChartTitle();
                FormatLegendSlide();
                FormatPlotAreaSlide();
            }
            else
            {
                FormatAxesLayout();
                RemoveChartTitle();
                FormatLegend();
                FormatPlotArea();
            }           

        }        

        public void FormatSeriesLanguage2()
        {
            foreach (var ser in _series)
            {
                ser.SetHeadingReferencesLang2();
            }
        }       

        public void FormatSeriesBarGap()
        {
            var ct = _oldChart.PlotArea.ChartTypes.Select(x => x.GetHashCode()).Distinct();

            foreach (var ctH in ct)
            {
                var chtTyp = _oldChart.PlotArea.ChartTypes.Where(x => x.GetHashCode().Equals(ctH)).First();

                if (chtTyp is ExcelBarChart)
                {
                    var c = chtTyp as ExcelBarChart;
                    c.GapWidth = 50;
                }
            }
        }

        private void RemoveChartTitle()
        {            
            if (_oldChart.Title != null)
            {
                if (!string.IsNullOrEmpty(_oldChart.Title.Text))
                {
                    _newWorkSheet.Cells[2, 3].Value = _oldChart.Title.Text;
                    _newWorkSheet.Cells[2, 4].Value = "<translate>";
                }                
                var x = _oldChart.Title.TopNode.ParentNode.SelectSingleNode("c:autoTitleDeleted", _oldChart.NameSpaceManager);

                if (x != null)
                {
                    _oldChart.Title.TopNode.ParentNode.RemoveChild(x);
                }

                _oldChart.Title.TopNode.ParentNode.RemoveChild(_oldChart.Title.TopNode);

            }           
        }        
    }
}
