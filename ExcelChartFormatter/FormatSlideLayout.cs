﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using OfficeOpenXml;
using OfficeOpenXml.Drawing.Chart;
using static FormatExcel.Extensions;

namespace FormatExcel
{
    public partial class ChartFormatter
    {

        private void FormatLegendSlide()
        {
            if (_oldChart.Legend.TopNode == null)
            {
                _oldChart.Legend.Add();
            }
            else
            {
                _oldChart.Legend.DeleteAllNode("c:layout");
            }

            _oldChart.Legend.CreateComplexNode("c:layout/c:manualLayout/c:xMode/@val='edge'");
            _oldChart.Legend.CreateComplexNode("c:layout/c:manualLayout/c:yMode/@val='edge'");
            _oldChart.Legend.CreateComplexNode("c:layout/c:manualLayout/c:x/@val='0'");
            _oldChart.Legend.CreateComplexNode("c:layout/c:manualLayout/c:y/@val='0.75'");
            _oldChart.Legend.CreateComplexNode("c:layout/c:manualLayout/c:w/@val='1'");
            _oldChart.Legend.CreateComplexNode("c:layout/c:manualLayout/c:h/@val='0.18974918650605008'");
            _oldChart.Legend.CreateComplexNode("c:legendPos/@val='t'");
            _oldChart.Legend.CreateComplexNode("c:overlay/@val='0'");                        
            _oldChart.Legend.CreateComplexNode("c:txPr/a:bodyPr/@anchorCtr='1'/@anchor='ctr'/@wrap='square'/@vert='horz'/@vertOverflow='ellipsis'/@spcFirstLastPara='1'/@rot='0'");
            //_oldChart.Legend.CreateComplexNode("c:txPr/a:lstStyle");
            _oldChart.Legend.DeleteNode("c:txPr/a:p/a:pPr/a:defRPr");
            _oldChart.Legend.CreateComplexNode("c:txPr/a:p/a:pPr/a:defRPr/@baseline='0'/@sz='1400'");
            _oldChart.Legend.CreateComplexNode("c:txPr/a:p/a:pPr/a:defRPr/a:solidFill/a:srgbClr/@val='000000'");
            _oldChart.Legend.CreateComplexNode("c:txPr/a:p/a:pPr/a:defRPr/a:latin/@charset='0'/@pitchFamily='34'/@typeface='Calibri'"); 
            _oldChart.Legend.CreateComplexNode("c:txPr/a:p/a:endParaRPr/@lang='et-EE'");

        }

        private void FormatPlotAreaSlide()
        {
            if (_oldChart.PlotArea.TopNode == null)
            {
                return;
            }

            _oldChart.PlotArea.DeleteAllNode("c:layout");
            _oldChart.PlotArea.CreateComplexNode("c:layout/c:manualLayout/c:layoutTarget/@val='inner'");
            _oldChart.PlotArea.CreateComplexNode("c:layout/c:manualLayout/c:xMode/@val='edge'");
            _oldChart.PlotArea.CreateComplexNode("c:layout/c:manualLayout/c:yMode/@val='edge'");
            _oldChart.PlotArea.CreateComplexNode("c:layout/c:manualLayout/c:x/@val='0.05'");
            _oldChart.PlotArea.CreateComplexNode("c:layout/c:manualLayout/c:y/@val='0'");
            _oldChart.PlotArea.CreateComplexNode("c:layout/c:manualLayout/c:w/@val='0.93'");
            _oldChart.PlotArea.CreateComplexNode("c:layout/c:manualLayout/c:h/@val='0.65'");
            _oldChart.PlotArea.DeleteAllNode("c:spPr");
            _oldChart.PlotArea.CreateComplexNode("c:spPr/a:noFill");
            _oldChart.PlotArea.CreateComplexNode("c:spPr/a:ln/a:noFill");
        }

        private void FormatAxesLayoutSlide()
        {
            var axHash = _axes.GetAll().Select(x => x.Id).Distinct();

            foreach (var h in axHash)
            {
                var ax = _axes.GetAll().Where(x => x.Id.Equals(h)).First();

                ax.DeleteNode("c:minorGridlines");
                ax.DeleteNode("c:majorGridlines");
                ax.SetXmlNodeString("c:majorTickMark/@val", "none");
                ax.SetXmlNodeString("c:minorTickMark/@val", "none");
                ax.SetXmlNodeString("c:tickLblPos/@val", "low");

                ax.DeleteNode("c:txPr");
                ax.DeleteNode("c:spPr");

                ax.CreateComplexNode("c:txPr/a:bodyPr");
                ax.CreateComplexNode("c:txPr/a:lstStyle");
                ax.CreateComplexNode("c:txPr/a:p/a:pPr/a:defRPr/@baseline='0'/@sz='1300'");
                ax.CreateComplexNode("c:txPr/a:p/a:pPr/a:latin/@pitchFamily='34'/@charset='0'/@typeface='Calibri'");
                ax.CreateComplexNode("c:txPr/a:p/a:pPr/a:solidFill/a:sysClr/@val='windowText'/@lastClr='000000'");

                ax.LabelPosition = eTickLabelPosition.NextTo;

                if (ax.TopNode.Name.Equals("c:valAx"))
                {
                    if (ax.AxisPosition == eAxisPosition.Left || ax.AxisPosition == eAxisPosition.Bottom || (ax.AxisPosition == eAxisPosition.Right && _axes.GetAll().Where(x => x.AxisPosition == eAxisPosition.Left).Count() == 0) || (ax.AxisPosition == eAxisPosition.Top && _axes.GetAll().Where(x => x.AxisPosition == eAxisPosition.Bottom).Count() == 0))
                    {
                        ax.CreateComplexNode("c:majorGridlines/c:spPr/a:ln/@w='3175'/a:solidFill/a:schemeClr/@val='bg1'/a:lumMod/@val=75000");
                    }

                    if (ax.Format == null || ax.Format.Equals("General"))
                    {
                        if ((ax.MaxValue != null && Math.Abs((double)ax.MaxValue) / 10 > 1) || (ax.MinValue != null && Math.Abs((double)ax.MinValue) / 10 > 1))
                        {
                            ax.Format = "0";
                        }
                        else
                        {
                            ax.Format = "0.0";
                        }
                    }
                }

                if (!ax.TopNode.Name.Equals("c:valAx"))
                {
                    ax.CreateComplexNode("c:spPr/a:noFill");
                    ax.CreateComplexNode("c:spPr/a:ln/@w=15875/a:solidFill/a:schemeClr/@val=tx1");
                    ax.CreateComplexNode("c:spPr/a:effectLst");

                    ax.MinorTickMark = eAxisTickMark.None;
                    ax.MajorTickMark = eAxisTickMark.In;
                }
                else
                {
                    ax.CreateComplexNode("c:spPr/a:noFill");
                    ax.CreateComplexNode("c:spPr/a:ln/a:noFill");
                }
            }


        }
    }
}
