﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Text;

namespace EPPlusStandard.Models
{
    public class MyExcelRange
    {
        ExcelWorksheet _worksheet;
        int _rowStart;
        int _columnStart;
        int _rowEnd;
        int _columnEnd;
        string _numberFormat;

        public MyExcelRange(ExcelRange EppRange, string FormatCode)
        {
            _worksheet = EppRange.Worksheet;
            _rowStart = EppRange.Start.Row;
            _rowEnd = EppRange.End.Row;
            _columnStart = EppRange.Start.Column;
            _columnEnd = EppRange.End.Column;
            _numberFormat = FormatCode;
        }

        public MyExcelRange(ExcelWorksheet WorkSheet, int RowStart, int ColumnStart, int RowEnd, int ColumnEnd, string FormatCode)
        {
            _worksheet = WorkSheet;
            _rowStart = RowStart;
            _rowEnd = RowEnd;
            _columnStart = ColumnStart;
            _columnEnd = ColumnEnd;
            _numberFormat = FormatCode;
        }

        public ExcelRange EppRange { get {return new ExcelRange(_worksheet, _rowStart, _columnStart, _rowEnd, _columnEnd); }  private set { } }        

        public string NumberFormat()
        {
            if (_numberFormat == null)
                return "General";

            return _numberFormat;
        }

        public override string ToString()
        {
            return new ExcelRange(_worksheet, _rowStart,  _columnStart, _rowEnd, _columnEnd).FullAddress.ToString();
        }
    }
}

// obsolete below
//public ExcelRange OldExcelRange { get; set; }
//public bool IsVertical { get; set; }
