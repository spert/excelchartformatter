﻿using System;
using System.Collections.Generic;

namespace EPPlusStandard.Services
{
    interface IService<TEntity> where TEntity : class
    {
        IList<TEntity> GetAll();

        TEntity Get(int id);
    }
}