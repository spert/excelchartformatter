﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPPlusStandard.Models
{
    public class MyCategory
    {
        MyExcelRange _newRange;
        string _oldRangeStr;
        
        public MyCategory(MyExcelRange NewRange, string OldRangeStr)
        {
            _newRange = NewRange;
            _oldRangeStr = OldRangeStr;
        }

        public MyExcelRange MyNewRange { get { return _newRange; } private set { } }

        public string OldRangeStr { get { return _oldRangeStr; } private set { } }        

        public virtual string FormatCode { get; set; }

        
        public override bool Equals(object obj)
        {
            return OldRangeStr.Equals(((MyCategory)obj).OldRangeStr.ToString());
        }

               
        public int GetColumnsCount()
        {
            return _newRange.EppRange.Columns;
        }
    }

}
